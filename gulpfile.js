const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const jsminify = require('gulp-js-minify');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');

const paths = {
  src: {
    html: 'src/index.html',
    js: 'src/js/**/*.js',
    scss: 'src/scss/**/*.scss',
    img: 'src/img/**/*.*',
  },
  dist: {
    root: 'dist',
    js: 'dist/js',
    css: 'dist/css',
    img: 'dist/img',
  },
};

// Clean dist folder
function cleanDist() {
  return gulp.src(paths.dist.root, { allowEmpty: true, read: false })
    .pipe(clean());
}

// Compile SCSS to CSS
function compileSass() {
  return gulp.src(paths.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS({ compatibility: 'ie11' }))
    .pipe(gulp.dest(paths.dist.css))
    .pipe(browserSync.stream());
}

// Concatenate and minify JS
function minifyJS() {
  return gulp.src(paths.src.js)
    .pipe(concat('scripts.min.js'))
    .pipe(jsminify())
    .pipe(gulp.dest(paths.dist.js))
    .pipe(browserSync.stream());
}

//Minify images
  const  ImageMin = () => (
	gulp.src('src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
);


// Serve and watch
function serve() {
    browserSync.init({
      server: {
        baseDir: [paths.dist.root, "./"], 
      },
    });
    gulp.watch(paths.src.html).on('change', browserSync.reload);
    gulp.watch(paths.src.js, gulp.series(minifyJS));
    gulp.watch(paths.src.scss, gulp.series(compileSass));
  }
  

// Export tasks
exports.clean = cleanDist;
exports.build = gulp.series(cleanDist, gulp.parallel(compileSass, minifyJS, ImageMin));
exports.dev = gulp.series(exports.build, serve);